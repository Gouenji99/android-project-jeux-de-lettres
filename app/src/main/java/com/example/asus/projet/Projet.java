package com.example.asus.projet;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Projet extends AppCompatActivity {

    Button b_inscri,b_login,jouer,b_aide;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projet);

        b_inscri=findViewById(R.id.btn_inscription);
        b_login=findViewById(R.id.btn_log);
        jouer=findViewById(R.id.btn_jouer);
        b_aide=findViewById(R.id.btn_aide);


        jouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_niveau= new Intent(Projet.this,Niveau_de_jeu.class);
                startActivity(go_niveau);

            }
        });

        b_inscri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_inscri= new Intent(Projet.this,Inscription.class);
                startActivity(go_inscri);

            }
        });

       b_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_login= new Intent(Projet.this,Login.class);
                startActivity(go_login);

            }
        });

    }
    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void help (View v) {
        String msg="Principe du jeu:\n" +
                "Le joueur doit trouver le mot valide le plus long à partir d’un ensemble de lettres choisies arbitrairement. \n Un mot est valide s’il appartient à une liste de mots saisie auparavant(Dictionnaire)";
        showMessage("HELP",msg);
    }

}
