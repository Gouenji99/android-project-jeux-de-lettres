package com.example.asus.projet;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.example.asus.projet.R.id.btn_inscri;
import static com.example.asus.projet.R.id.editText2;

public class Inscription extends AppCompatActivity {
    Button inscrire;
    EditText edt1, edt2, edt3;
    DataBase Db;
    Intent p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        Db = new DataBase(this);
        p= new Intent(this, Inscription.class);
        inscrire = findViewById(R.id.btn_inscri);
        edt1 = findViewById(R.id.editText);
        edt2 = findViewById(R.id.editText2);
        edt3 = findViewById(R.id.editText3);
        inscrire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(search(edt2.getText().toString())==-1 && search(edt1.getText().toString())==-1 )
                {
                    boolean Insert = Db.insertData(edt2.getText().toString(),edt1.getText().toString(), edt3.getText().toString()

                    );

                        if(Insert == true)
                        { Toast.makeText(Inscription.this,"Data ajoutée",Toast.LENGTH_LONG).show();
                            startActivityForResult(p,0);}
                        else
                            Toast.makeText(Inscription.this,"Data non ajoutée",Toast.LENGTH_LONG).show();
                    }else
                        {Toast.makeText(Inscription.this," verifier mot de passe",Toast.LENGTH_LONG).show();}
                if (edt1.length() == 0 || (edt2.length() == 0) || (edt3.length() == 0)) {
                    Toast.makeText(Inscription.this, "champ vide", Toast.LENGTH_SHORT).show();
                } else {
                    Intent go_jeu = new Intent(Inscription.this, Jeu.class);
                    startActivity(go_jeu);
                }
            }
        });
    }

    public int search (String ch) {
        //String ch=editName.getText().toString();
        Cursor res = Db.getAllData();
        if (res.getCount() == 0) {
            // show message
            showMessage("Error", "Nothing found");
        }

        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {

            buffer.append("Name :"+ res.getString(1)+"\n");

        }
        int n=  buffer.indexOf(ch);

        if(n!=-1)
            Toast.makeText(Inscription.this,"le nom existe déjà",Toast.LENGTH_LONG).show();

        return n;

    }
    public void showMessage(String title,String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}

