package com.example.asus.projet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.util.ArrayList;

public class Niveau_de_jeu extends AppCompatActivity {
    private RadioGroup radioGroup1,radioGroup2;
    private Button btn_ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_niveau_de_jeu);
        radioGroup1=findViewById(R.id.radioGroup1);
        radioGroup2=findViewById(R.id.radioGroup2);
        btn_ok=findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radio_selected1 = radioGroup1.getCheckedRadioButtonId();
                int radio_selected2 = radioGroup2.getCheckedRadioButtonId();
                RadioButton nbr_lettres = findViewById(radio_selected1);
                RadioButton temps = findViewById(radio_selected2);
                Intent go_jeu = new Intent(Niveau_de_jeu.this,Jeu.class);
                go_jeu.putExtra("nbr_l",nbr_lettres.getText().toString());
                go_jeu.putExtra("nbr_t",temps.getText().toString());
                startActivity(go_jeu);
                }
        });

    }
}
