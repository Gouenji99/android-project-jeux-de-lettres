package com.example.asus.projet;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class Login extends AppCompatActivity {
    public static int i;
    Button login,retour;
    EditText edt4,edt5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login=findViewById(R.id.btn_login);
        retour=findViewById(R.id.btn_retour);
        edt4=findViewById(R.id.editText4);
        edt5=findViewById(R.id.editText5);
        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_projet= new Intent(Login.this,Projet.class);
                startActivity(go_projet);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt4.length()==0||(edt5.length()==0))
                {
                    Toast.makeText(Login.this,"champ vide",Toast.LENGTH_SHORT).show();
                }
                else {

                    Intent go_jeu = new Intent(Login.this, Jeu.class);
                    startActivity(go_jeu);
                }
            }
        });
    }
    public void valider(View view) {
        DataBase myDb;
        myDb = new DataBase(this);
        Cursor res = myDb.getAllData();
        if(res.getCount() == 0) {
            showMessage("Error","Nothing found");
        }
        StringBuffer bn = new StringBuffer();
        StringBuffer bp = new StringBuffer();
        ArrayList<String> indnom=new ArrayList();
        ArrayList<String> indpass=new ArrayList();
        while (res.moveToNext()) {

            bn.append("Name :"+ res.getString(1)+"\n");
            bp.append("pass :"+ res.getString(2)+"\n");
            indnom.add(res.getString(1));
            indpass.add(res.getString(2));


        }
        String ch1,ch2;
        ch1=edt4.getText().toString();
        ch2=edt5.getText().toString();
        if(!indnom.contains(ch1))
        {Toast.makeText(Login.this,"nom n'existe pas ",Toast.LENGTH_LONG).show();}
        else{ int n=indnom.indexOf(ch1);
            String bpo=indpass.get(n);
            i=n;
            if(!bpo.equals(ch2)){Toast.makeText(Login.this,"mot pass invalide"+bpo,Toast.LENGTH_LONG).show();}
            else {

                Intent l=new Intent(this,Jeu.class);
                startActivityForResult(l,0);
            }

        }
    }
    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}
