package com.example.asus.projet;

import android.app.AlertDialog;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.example.asus.projet.Login.i;

public class Jeu extends AppCompatActivity {
Button valider,cons,voy;
EditText edt6;
TextView txtvv;
public String [] voyelle = {"A","E","Y","U","I","O"};
public String [] consonne = {"Z","R","T","P","Q","S","D","F","G","H","J","K","L","M","W","X","C","V","B","N"};
    int    clickcount=0;
    int nbv=0,nbc=0;
    String lettres="";
    int temps;
    int  nombre_lettres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);
        voy=findViewById(R.id.voyelle);
        cons=findViewById(R.id.consonne);
        valider=findViewById(R.id.valider);
        edt6=findViewById(R.id.editText6);
        String nbr_lettres = getIntent().getStringExtra("nbr_l");
        String nbr_temps = getIntent().getStringExtra("nbr_t");
        temps = Integer.parseInt(nbr_temps);
        nombre_lettres = Integer.parseInt(nbr_lettres);
        }
    public void CONS(View view) {
        txtvv=findViewById(R.id.textViewjeu);
        nbc++;
        clickcount =nbc+nbv;
         if(clickcount <=nombre_lettres)
        { Random rand = new Random();

            Toast.makeText(this,"compter lettres"+clickcount,Toast.LENGTH_LONG).show();
            int n = rand.nextInt(consonne.length);
            lettres = lettres + consonne[n].toUpperCase();

            if (clickcount == 1)
                txtvv.setText(consonne[n].toUpperCase() + " ");
            else {
                txtvv.append(" ");
                txtvv.append(consonne[n].toUpperCase());

            }}
    }
    public void VOY(View view){

        nbv++;
        clickcount =nbc+nbv;
        if(clickcount <= nombre_lettres)
        {  Toast.makeText(this,"compter lettres"+clickcount,Toast.LENGTH_LONG).show();
            txtvv=findViewById(R.id.textViewjeu);
            Random rand = new Random();
            int n = rand.nextInt(voyelle.length);
            lettres = lettres + voyelle[n].toUpperCase();
            clickcount = clickcount + 1;
            if (clickcount == 1)
                txtvv.setText(voyelle[n].toUpperCase() + " ");
            else {
                txtvv.append(" ");
                txtvv.append(voyelle[n].toUpperCase());

            }
        }
    }
    public void VERIF(View view) {
        EditText ed =(EditText)findViewById(R.id.editText6);
        String l=ed.getText().toString().toUpperCase();
        ArrayList<String> mots2=getlistofword();

        boolean b=existe(l,lettres);
        if(b==false)
            Toast.makeText(this,"tu n'a pas utiliser correctement les lettres",Toast.LENGTH_LONG).show();
        else
        {



            ArrayList resulta=new ArrayList();
            int j=0;
            int o=0;
            for(int i=0;i<mots2.size();i++)
            {  List<String> L = new ArrayList<String>(Arrays.asList(mots2.get(i).split(" ")));
                if (existe(L.get(0),lettres))
                {
                    resulta.add(L.get(0));
                }


            }

            StringBuffer buffer = new StringBuffer();
            while (j<resulta.size())
            {
                if(resulta.get(j).toString().length()==nombre_lettres)
                    buffer.append(resulta.get(j)+"\n");
                j++;

            }
            if(resulta.contains(l))
                showMessage("TU AS GAGNE","MOTS \n\n\n"+buffer.toString());
            else
                Toast.makeText(this,"mot n'existe pas",Toast.LENGTH_LONG).show();
            int score=(nombre_lettres/l.length())*100;
            Toast.makeText(this,"votre score est "+score,Toast.LENGTH_LONG).show();
            ArrayList<String> indid=new ArrayList();//fih el ID
            DataBase Db = new DataBase(this);
            Cursor res = Db.getAllData();
            while (res.moveToNext()) {

                indid.add(res.getString(0));

            }
            String cc = indid.get(i);
        }
    }
    public  boolean existe(String d,String l)
    {
        int i=0;
        boolean b=true;
        String l2=l;
        StringBuilder sb=new StringBuilder(l);
        while((i<d.length())&&(b==true))
        {

            String lettre=Character.toString(d.charAt(i));

            if(l2.contains(lettre))
            {
                i++;
                int del=sb.indexOf(lettre);
                sb.deleteCharAt(del);
                l2=sb.toString();


            }else {b=false;}
        }

        return b;


    }
    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
    public ArrayList<String> getlistofword()
    {   ArrayList<String> listdic = new ArrayList<String>();
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(getAssets().open("dictionnaire.txt")));
            String line;
            while (((line = buffer.readLine()) != null))
            {
                listdic.add(line);

            }}
        catch (Exception e) {
            e.printStackTrace();
        }
        return  listdic;

    }
}
